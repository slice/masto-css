# Mastodon CSS… stuff

Attempts to address gripes with Mastodon using only CSS, the most passive-aggressive language.

These are all compatible with [Dizzydon](https://github.com/dizzy-labs/dizzydon), btw.
It disables the stretchy columns but you probably wanted it to.

Also works with glitchsoc's version of the frontend, now that Pleroma's upgraded to that.

## Anti-Clickjacking

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/feed-noclick.user.css)

Accessibility aid that disables clicking buttons on posts unless you hover over them for a bit.
Solves the *very* common complaint of misaimed clicks caused by the feed updating under the pointer.

## Dense Column Layout

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/dense-columns.user.css)

Reformats the UI so you can get more good stuff on the screen:

* Columns stretch to the screen width.
  If you have more than will reasonably fit, it'll scroll as usual.
* On sufficiently tall screens you get an extra column space under the message box.
  You can stick your notifications there, Pleroma style.

## Vague Mode

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/numberless.user.css)

Adds options to remove distracting numbers (post/user stats).

## Miscellaneous Fixes

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/bugfixes.user.css)

I'll add them as I find them.

## Dense Post Layout (WIP)

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/dense-posts.user.css)

Messes up the posts significantly for dubious gains.
The settings are mislabeled and don't work right yet, please ignore them.
Mostly has no effect with glitchsoc; that already has a much better compact post layout.

* Removes the left margin and moves avatars to the right to wrap text around
* Long paragraphs take up slightly less vertical space
* Image thumbnails are slightly larger and take up slightly more vertical space

# Build Dependencies (for changing stuff)

1. Run `apt install webpack` (or distro equivalent).
2. Examine the dependencies. When it prompts you to continue, say **N**. Fuck that noise.
3. Install [`sassc`](https://github.com/sass/libsass) and `make` (if you somehow don't have it).

# Hacking

Just edit the `src/*.scss` files and run `make`. Output is dumped in the top level directory.

# License

All files here are under the GNU Affero General Public License 3.0,
go ahead and steal bits of them for your own instance if you want.

[install-button]: https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg
