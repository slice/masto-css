SASS = sassc

all: $(patsubst %.scss,%.user.css,$(notdir $(wildcard src/*.scss))) extras/FontAwful.user.css

clean:
	rm -f *.user.css

%.user.css: src/%.scss
	$(SASS) src/$*.scss $@

extras/%.user.css: extras/%.scss
	$(SASS) extras/$*.scss $@
